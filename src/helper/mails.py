from django.core.mail import send_mail, send_mass_mail
from django.template import Context
from django.template.loader import get_template, render_to_string

from eyureka import settings


def send(subject, template, context, recipient):
    body = render_to_string(template, context)

    return send_mail(subject, body, settings.EMAIL_HOST_USER, recipient, fail_silently=False)

def send_mass(subject, template, context_recipients):
    messages = []
    for cont_recip in context_recipients:
        body = render_to_string(template, cont_recip.get('context', {}))
        messages.append(
            (subject, body, settings.EMAIL_HOST_USER, cont_recip.get('recipient', []))
        )

    return send_mass_mail(messages, fail_silently = False)