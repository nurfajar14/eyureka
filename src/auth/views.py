from django.db import transaction
from django.db.models.functions import Lower
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.views import APIView

from src.account.models import Contact, AccountContact
from src.account.serializers import AccountSerializers, ContactSerializers, AccountContactSerializers
from src.auth.serializers import AuthSerializers

# Create your views here.
from src.group.models import Group
from src.helper.auth import check_password, generate_token
from src.helper.mails import send
from src.helper.response import response


class LoginView(APIView):
    def post(self, request):
        valid_request = AuthSerializers(data=request.data)
        if not valid_request.is_valid():
            return response(status.HTTP_400_BAD_REQUEST, message="Get token failed", errors=valid_request.errors)

        email = valid_request.data.get('email')
        password = valid_request.data.get('password')

        acc_contact = get_object_or_404(AccountContact, contact__email=email)
        if not acc_contact.account.is_active:
            return response(status.HTTP_400_BAD_REQUEST, message='Akun belum aktif, mohon cek email untuk aktivasi')

        if not check_password(acc_contact.contact.password, password):
            return response(status.HTTP_400_BAD_REQUEST, message='Invalid password')

        result = generate_token(acc_contact)

        return response(status.HTTP_200_OK, result, message='Login Success', status=True)

    def handle_exception(self, exc):
        return response(status.HTTP_404_NOT_FOUND, message='login failed', errors={'error': str(exc)})


class Register(APIView):
    @transaction.atomic()
    def post(self, request):
        data = request.data
        email = data.get('contact',{}).get('email', '').lower()
        exist_contact = AccountContact.objects.annotate(email_lower=Lower('contact__email')).filter(email_lower=email)
        if exist_contact:
            return response(status.HTTP_400_BAD_REQUEST, message="Account already exist")

        account = AccountSerializers(data=data.get('account'))
        if not account.is_valid():
            return response(status.HTTP_400_BAD_REQUEST, message="Create account failed", errors=account.errors)

        sid = transaction.savepoint()
        account.save()

        payload_contact = data.get("contact", {})
        contact = ContactSerializers(data=payload_contact)
        if not contact.is_valid():
            transaction.savepoint_rollback(sid)
            return response(status.HTTP_400_BAD_REQUEST, message="Create contact failed", errors=contact.errors)

        contact.save(group=Group.objects.get(id=3))

        account_contact = AccountContactSerializers(data={"account":account.instance.id, "contact":contact.instance.id})
        if not account_contact.is_valid():
            transaction.savepoint_rollback(sid)
            return response(status.HTTP_400_BAD_REQUEST, message="Create account and contact failed",
                            errors=account_contact.errors)

        account_contact.save()

        transaction.savepoint_commit(sid)

        #kirim email
        send(
            subject="Registrasi tenant Eyureka",
            template="mail/register.html",
            context={
                "account_name": account.instance.name,
                "url": "localhost:8000/account/activate?accountId={}".format(account.instance.id)},
            recipient=[contact.instance.email]
        )

        return response(status.HTTP_201_CREATED, message="Success", status=True)

    def handle_exception(self, exc):
        return response(status.HTTP_404_NOT_FOUND, message='register failed', errors={'error': str(exc)})