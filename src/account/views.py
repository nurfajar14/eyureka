from functools import partial

from django.db import transaction
from django.db.models.functions import Lower
from django.shortcuts import render, get_object_or_404

# Create your views here.
from rest_framework import status
from rest_framework.views import APIView

from src.account.models import AccountContact, Account, Contact
from src.account.serializers import ContactSerializers, AccountContactSerializers
from src.helper.auth import login_required, hash_password, generate_password
from src.helper.mails import send_mass
from src.helper.response import response


class Invite(APIView):
    @partial(login_required, module=6, access="CREATE")
    def post(self, request, account_id, **kwargs):
        account = get_object_or_404(Account, id=account_id)
        contact = ContactSerializers(data=request.data, many=True)
        if not contact.is_valid():
            return response(status.HTTP_400_BAD_REQUEST, message="Invite contact failed", errors=contact.errors)

        mails = list(map(lambda x: x["email"].lower(), contact.initial_data))
        exist_contacts = list(Contact.objects.filter(email__in=mails).values_list("email",flat=True))
        if exist_contacts:
            return response(status.HTTP_400_BAD_REQUEST,
                            message="Invite contact failed, {} already exist".format(exist_contacts))

        password = hash_password(generate_password())

        sid = transaction.savepoint()
        contact.save(password=password)

        data_acc_contact = list(map(lambda x: {"account":account.id, "contact":x["id"]}, contact.data))
        account_contact = AccountContactSerializers(data=data_acc_contact, many=True)
        if not account_contact.is_valid():
            transaction.savepoint_rollback(sid)
            return response(status.HTTP_400_BAD_REQUEST, message="Create account and contact failed",
                            errors=account_contact.errors)

        account_contact.save()
        transaction.savepoint_commit(sid)

        context_recipients = list(map(lambda x: {
            'context': {
                "account_name": x.name,
                "url": "localhost:8000/account/activate?accountId={}".format(account.id)
            },
            'recipient': [x.email]
        }, contact.instance))

        send_mass(subject="Registrasi tenant Eyureka",
                  template="mail/register.html",
                  context_recipients=context_recipients)

        return response(status.HTTP_201_CREATED, message="Success", status=True)

    def handle_exception(self, exc):
        return response(status.HTTP_404_NOT_FOUND, message='Group not found', errors={'error': str(exc)})


class Activate(APIView):
    def post(self, request):
        account = get_object_or_404(Account, id=request.data.get('accountId'))
        if account.is_active:
            return response(status.HTTP_400_BAD_REQUEST, message='Account already activated')

        account.is_active = True
        account.save()

        return response(status.HTTP_200_OK, message="Success", status=True)

    def handle_exception(self, exc):
        return response(status.HTTP_404_NOT_FOUND, message='Account not found', errors={'error': str(exc)})
