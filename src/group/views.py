from functools import partial
from django.db import transaction
from rest_framework import status
from rest_framework.views import APIView
from src.group.serializers import GroupSerializers, AccountGroupSerializers, RoleSerializers
from src.helper.auth import login_required
from src.helper.response import response


class GroupViews(APIView):
    @partial(login_required, module=6, access="CREATE")
    @transaction.atomic()
    def post(self, request, **kwargs):
        # create group
        group = GroupSerializers(data=request.data)
        if not group.is_valid():
            return response(status.HTTP_400_BAD_REQUEST, message="Create group failed", errors=group.errors)

        sid = transaction.savepoint()
        group.save()

        #sementara ngga dipake, asumsi semua tenant punya group yang sama
        # # create account group
        # account_group = AccountGroupSerializers(data={"account": kwargs.get("accountId"), "group": group.instance.id})
        # if not account_group.is_valid():
        #     transaction.savepoint_rollback(sid)
        #     return response(status.HTTP_400_BAD_REQUEST, message="Create account group failed",
        #                     errors=account_group.errors)
        #
        # account_group.save()

        # create role
        role = RoleSerializers(data=request.data.get("roles"), many=True)
        if not role.is_valid():
            transaction.savepoint_rollback(sid)
            return response(status.HTTP_400_BAD_REQUEST, message="Create account group roles", errors=role.errors)

        role.save(group=group.instance)
        transaction.savepoint_commit(sid)

        return response(status.HTTP_201_CREATED, message="Success", status=True)

    def handle_exception(self, exc):
        return response(status.HTTP_404_NOT_FOUND, message='Group not found', errors={'error': str(exc)})
