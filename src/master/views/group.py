from rest_framework import status
from rest_framework.views import APIView

from src.group.models import Group
from src.group.serializers import GroupSerializers
from src.helper.response import response


class GroupView(APIView):
    def get(self, request):
        group = GroupSerializers(Group.objects.all(), many=True)

        return response(status.HTTP_200_OK, data=group.data, message="Group", status=True)

    def handle_exception(self, exc):
        return response(status.HTTP_404_NOT_FOUND, message='Group not found', errors={'error': str(exc)})